#include <iostream>
#include <string>

int main()
{
    std::string word("Procrastination");
    // ������ char ���������� � 0, ������� ����� ������� ������� ����� 0
    // � ���������� (����� ������ - 1);
    char first = word[0];
    char last = word[word.length() - 1];

    std::cout << "String output - " << word << "\n";
    std::cout << "String length - " << word.length() << "\n";
    std::cout << "First symbol - " << first << "\n" << "Last symbol - " << last << "\n";

    return 0;
}